const adalGraphApi = require('./../src/index');

require('dotenv').config();
const _ = require('lodash');
const colors = require('colors');
const fs = require('fs');

const main = async () => {

  // Server to Server
  const { getToken, getGroups, getMembersOfGroup } = adalGraphApi({
    tenantId: _.get(process.env, 'AZURE_TENANT_ID'),
    appId: _.get(process.env, 'AZURE_CLIENT_ID'),
    clientSecret: _.get(process.env, 'AZURE_CLIENT_SECRET'),
    // scope: 'https://graph.microsoft.com/.default', // .default is for all permissions granted for specific app
    // grantType: 'client_credentials', // No explanation, docs says "It must be client_credentials"
    // verbose: false,
  });

  // Get application/client token
  const token = await getToken();
  // console.log(token);
  fs.writeFileSync('./data/token.txt', token, 'utf-8');

  // Get the groups in AD
  const groups = await getGroups(token);

  fs.writeFileSync('./data/groups.json', JSON.stringify(groups, null, 2), 'utf-8');

  const filteredGroups = groups.map((o) => {
    return {id: o.id, displayName: o.displayName, mail: o.mail, };
  });

  // Get users for specific group

  let totalUsers = [];
  let groupWithUsers = [];
  for (let i = 0; i < groups.length; i++) {
    const singleGroup = groups[i];
    const { id, displayName } = singleGroup;

    const users = await getMembersOfGroup(token, id);
    console.log(`Total users in group ${displayName.yellow} | ${id.red} :`, `${users.length}`.green);
    totalUsers = totalUsers.concat(users);

    for (let j = 0; j < users.length; j++) {
      const user = users[j];
      let displayName = _.get(user, 'displayName', 'NULL');
      let jobTitle = _.get(user, 'jobTitle', 'NULL');
      let mail = _.get(user, 'mail', 'NULL');
      let mobilePhone = _.get(user, 'mobilePhone', 'NULL');
      if (displayName === null) { displayName = 'NULL'; }
      if (jobTitle === null) { jobTitle = 'NULL'; }
      if (mail === null) { mail = 'NULL'; }
      if (mobilePhone === null) { mobilePhone = 'NULL'; }
      console.log(` |- ${displayName.toString().green} | ${jobTitle.toString().yellow} | ${mail.toString().yellow} | ${mobilePhone.toString().yellow} `);
    }

    groupWithUsers.push({
      ...singleGroup,
      users,
    });
  }

  console.log(`Total users in all groups :`, totalUsers.length);

  fs.writeFileSync('./data/totalUsers.json', JSON.stringify(totalUsers, null, 2), 'utf-8');
  fs.writeFileSync('./data/adalStructure.json', JSON.stringify(groupWithUsers, null, 2), 'utf-8');

};
main()
  .catch((error) => { console.log(error) });
