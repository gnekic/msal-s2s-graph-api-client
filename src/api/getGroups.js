const _ = require('lodash');
const request = require('request-promise-native');

module.exports = (settings) => {

  const getGroups = async (token, apiVersion = 'v1.0') => {

    const makeActualRequest = async (url) => {
      const requestOptions = {
        method: 'GET',
        url,
        json: true,
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      };
  
      // Call the request
      const apiResponse = await request(requestOptions)
        .then((response) => {
          return response;
        })
        .catch((error) => {
          if (settings.verbose) {
            console.log('[msal-s2s-graph-api-client] REQUEST ERROR');
            console.log(error);
          }
          return; // undefined if error
        });

      return apiResponse;
    };

    const apiResponse = await makeActualRequest(`https://graph.microsoft.com/${apiVersion}/groups`);
    let totalValue = [];

    const newValue = _.get(apiResponse, 'value', []);
    totalValue = totalValue.concat(newValue);

    let nextLink = _.get(apiResponse, '@odata.nextLink');
    const maxStepsToFollow = Infinity;
    let currentStep = 0;

    while (nextLink !== undefined && currentStep < maxStepsToFollow) {
      const nextApiResponse = await makeActualRequest(nextLink);
      nextLink = _.get(nextApiResponse, '@odata.nextLink');
      totalValue = totalValue.concat(_.get(nextApiResponse, 'value', []));
      currentStep++;
    }

    // Debug info out
    if (settings.verbose) {
      console.log('[msal-s2s-graph-api-client] API RESPONSE');
      console.log(JSON.stringify(apiResponse, null, 2));
    }

    // Return token
    return totalValue;
  }

  // Permissions: User.Read.All, Directory.Read.All
  // URL: https://docs.microsoft.com/en-us/graph/api/group-list-members?view=graph-rest-1.0

  // Return functions that have access to settings passed
  return getGroups;
};
