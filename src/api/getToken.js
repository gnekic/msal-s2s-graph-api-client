const _ = require('lodash');
const request = require('request-promise-native');

module.exports = (settings) => {
  // Documentation reference: https://docs.microsoft.com/en-us/graph/auth-v2-service

  // Function to get token
  const getToken = async () => {

    const { 
      tenantId,
      appId,
      scope,
      clientSecret,
      grantType,
    } = settings;

    const requestOptions = {
      method: 'POST',
      url: `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
      json: true,
      formData: {
        client_id: appId,
        scope: scope,
        client_secret: clientSecret,
        grant_type: grantType,
      },
    };

    // Call the request
    const apiResponse = await request(requestOptions)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        if (settings.verbose) {
          console.log('[msal-s2s-graph-api-client] REQUEST ERROR');
          console.log(error);
        }
        return; // undefined if error
      });
    
    // Debug info out
    if (settings.verbose) {
      console.log('[msal-s2s-graph-api-client] API RESPONSE');
      console.log(JSON.stringify(apiResponse, null, 2));
    }

    // Return token
    return _.get(apiResponse, 'access_token');
  }

  // Return functions that have access to settings passed
  return getToken;
};
