const getToken = require('./api/getToken');
const getGroups = require('./api/getGroups');
const getMembersOfGroup = require('./api/getMembersOfGroup');

// Load all api calls and send settings after validation
module.exports = (userSettings) => {
  const defaultSettings = {
    scope: 'https://graph.microsoft.com/.default', // .default is for all permissions granted for specific app
    grantType: 'client_credentials', // No explanation, docs says "It must be client_credentials"
    verbose: false,
  };
  // Valdate & override default settings
  const settings = {
    ...defaultSettings,
    ...userSettings,
  }
  // Pass settings and return functions
  return {
    getToken: getToken(settings),
    getGroups: getGroups(settings),
    getMembersOfGroup: getMembersOfGroup(settings),
  };
};
